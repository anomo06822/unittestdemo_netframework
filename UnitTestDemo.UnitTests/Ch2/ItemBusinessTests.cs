﻿using System;
using Ch1;
using Xunit;

namespace UnitTestDemo.UnitTests.Ch2
{
    public class ItemBusinessTests
    {
        [Fact]
        public void GetDefaultPrice_InputIphone_OutputTenPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var testItem = new TestItemFactory().Create(TestItemType.Iphone);
            var item = testItem.Item;
            var expected = this.GetItemExtendPrice(testItem);

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }



        [Fact]
        public void GetDefaultPrice_InputSamsung_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var testItem = new TestItemFactory().Create(TestItemType.Samsung);
            var item = testItem.Item;
            var expected = this.GetItemExtendPrice(testItem);

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputIphoneAndQtyIsSix_OutputSixtyPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var qty = 6;
            var testItem = new TestItemFactory().Create(TestItemType.Iphone, qty);
            var item = testItem.Item;
            var expected = this.GetItemExtendPrice(testItem);
            

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputSamsungAndQtyIsSix_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var qty = 6;
            var testItem = new TestItemFactory().Create(TestItemType.Iphone, qty);
            var item = testItem.Item;
            var expected = this.GetItemExtendPrice(testItem);

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(TestItemType.Iphone, 1)]
        [InlineData(TestItemType.Samsung, 1)]
        [InlineData(TestItemType.Iphone, 6)]
        [InlineData(TestItemType.Samsung, 6)]
        public void GetExtendPrices_InputMultipleData_OutputCorrectPrices(TestItemType itemType, int qty)
        {
            //arrange
            var testItem = new TestItemFactory().Create(itemType, qty);
            var item = testItem.Item;
            var expected = this.GetItemExtendPrice(testItem);
            var itemBusiness = new ItemBusiness();

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        private decimal GetItemExtendPrice(TestItem item)
        {
            return item.Qty * item.Price;
        }
    }
}
