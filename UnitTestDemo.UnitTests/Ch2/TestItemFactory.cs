﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static UnitTestDemo.UnitTests.Ch2.ItemBusinessTests;

namespace UnitTestDemo.UnitTests.Ch2
{
    public class TestItemFactory
    {
        public TestItem Create(TestItemType itemType, int qty = 1)
        {
            TestItem testItem = new TestItem();
            testItem.Qty = qty;

            switch (itemType)
            {
                case TestItemType.Iphone:
                    testItem.Item = "Iphone";
                    testItem.Price = 10m;
                    
                    break;
                case TestItemType.Samsung:
                    testItem.Item = "Samsung";
                    testItem.Price = 0m;
                    break;
                default:
                    break;
            }

            return testItem;
        }
    }
}
