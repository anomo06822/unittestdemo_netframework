﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDemo.Ch3.TestDouble;

namespace UnitTestDemo.UnitTests.Ch3.Fake
{
    public class FakeTimeWrapper : ITimeWrapper
    {
        internal DateTime MockTime;
        public DateTime Now => MockTime;
    }
}
