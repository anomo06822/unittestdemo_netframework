﻿using System;
using System.IO;
using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.UnitTests.Ch3.Spy
{
    /// <summary>
    /// spy 的範例，這邊為了跟 mock 區隔，會發現多了 SpyLogger 必且多了一些屬性進行記錄，而後面我們 assert 的時間就可以透過這方式驗證是否有互動
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;

        [Fact]
        public void ValidatePrice_PriceIsLessThanZero_WriteLog()
        {
            //arrange
            var price = -1m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            var spyLogger = new SpyLogger();
            var itemBusiness = new ItemBusiness(_priceCalculator, spyLogger);

            //act
            //assert
            Assert.Throws<InvalidDataException>(() => itemBusiness.ValidatePrice((price)));
            Assert.True(spyLogger.IsWrittenLog());
        }
    }
}
