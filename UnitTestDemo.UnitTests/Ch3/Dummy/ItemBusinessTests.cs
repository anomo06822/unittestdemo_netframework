﻿using System.IO;
using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.UnitTests.Ch3.Dummy
{
    /// <summary>
    /// Dummy 的範例，主要 Dummy 只是過程中的需要的步驟，對於 Return 的資訊並不重視
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;

        [Theory]
        [InlineData(5)]
        [InlineData(0)]
        public void ValidatePrice_PriceIsGreaterThanOrEqualToZero_ReturnTrue(decimal price)
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            var itemBusiness = new ItemBusiness(_priceCalculator);

            //act
            var actual = itemBusiness.ValidatePrice(price);

            //assert
            Assert.True(actual);
        }

        [Theory]
        [InlineData(-5)]
        public void ValidatePrice_PriceIsLessThanZero_ThrowInvalidDataException(decimal price)
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            var itemBusiness = new ItemBusiness(_priceCalculator);

            //act
            //assert
            Assert.Throws<InvalidDataException>(() => itemBusiness.ValidatePrice((price)));
        }
    }
}
