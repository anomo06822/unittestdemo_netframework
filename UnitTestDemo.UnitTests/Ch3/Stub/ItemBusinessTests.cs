﻿using System;
using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using UnitTestDemo.Model;
using Xunit;

namespace UnitTestDemo.UnitTests.Ch3.Stub
{
    /// <summary>
    /// Stub 的範例，這邊會發現多了 Return 的方法並且指定回傳結果，而且在 SUT 調用到我們指定的方法時，會發現如果 match 就會與我們當初設定的一樣，並影響 SUT 回傳結果。
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;

        [Fact]
        public void GetDefaultPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 1m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetDefaultPrice("test").Returns(1m);
            var itemBusiness = new ItemBusiness(_priceCalculator);
            //act
            var actual = itemBusiness.GetDefaultPrice("test");

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 1m;
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetExtendPrices("test", 1).Returns(1m);
            var itemBusiness = new ItemBusiness(_priceCalculator);

            //act
            var actual = itemBusiness.GetExtendPrices("test", 1);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}
