﻿using System;
using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using UnitTestDemo.Model;
using Xunit;

namespace UnitTestDemo.UnitTests.Ch3.Mock
{
    /// <summary>
    /// Mock 的範例，主要是驗證互動行為，所以這邊採用 Received(1) 的方式處理，會發現我們關注的是 PriceCalculator 是否被使用
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;

        [Fact]
        public void GetDefaultPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetDefaultPrice(string.Empty).Returns(0m);
            var itemBusiness = new ItemBusiness(_priceCalculator);
            //act
            itemBusiness.GetDefaultPrice(String.Empty);

            //assert
            _priceCalculator.Received(1).GetDefaultPrice(String.Empty);
        }

        [Fact]
        public void GetExtendPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceCalculator.GetExtendPrices(string.Empty, 0).Returns(0m);
            var itemBusiness = new ItemBusiness(_priceCalculator);
            //act
            itemBusiness.GetExtendPrices(string.Empty, 0);

            //assert
            _priceCalculator.Received(1).GetExtendPrices(string.Empty, 0);
        }
    }
}
