﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public class PriceCalculator : TestDouble.IPriceCalculator
    {
        public const string IPhone = "Iphone";

        public decimal GetDefaultPrice(string item)
        {
            return item == IPhone ? 10m : 0m;
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return this.GetDefaultPrice(item) * qty;
        }
    }
}
