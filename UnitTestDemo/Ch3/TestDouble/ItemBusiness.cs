﻿using System;
using System.IO;

namespace UnitTestDemo.Ch3.TestDouble
{
    public class ItemBusiness
    {
        private readonly IPriceCalculator _priceCalculator;
        private readonly ILogger _logger;

        public ItemBusiness(IPriceCalculator priceCalculator)
        {
            this._priceCalculator = priceCalculator;
            this._logger = new Logger();
        }

        public ItemBusiness(IPriceCalculator priceCalculator, ILogger logger)
        {
            this._priceCalculator = priceCalculator;
            this._logger = logger;
        }

        public decimal GetDefaultPrice(string item)
        {
            return GetPrices((x, _) => this._priceCalculator.GetDefaultPrice(x), item);
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return GetPrices((x, y) =>  this._priceCalculator.GetExtendPrices(x, y), item, qty);
        }

        public decimal GetPrices(Func<string, int, decimal> predicate, string item, int qty = 1)
        {
            var price = predicate(item, qty);

            ValidatePrice(price);

            return price;
        }

        public bool ValidatePrice(decimal price)
        {
            if (price >= 0)
            {
                return true;
            }

            this._logger.WriteLog("price is less than zero");
            throw new InvalidDataException("price is less than zero");
        }
    }
}
