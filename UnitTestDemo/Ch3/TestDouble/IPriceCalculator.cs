﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public interface IPriceCalculator
    {
        decimal GetDefaultPrice(string item);
        decimal GetExtendPrices(string item, int qty);
    }
}