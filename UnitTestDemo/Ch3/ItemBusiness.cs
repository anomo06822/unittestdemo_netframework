﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnitTestDemo.Infrastructure.Helper;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class ItemBusiness
    {
        private readonly IPriceCalculator _priceCalculator;
        private readonly IItemDA _itemDa;

        public ItemBusiness(IPriceCalculator priceCalculator, IItemDA itemDa)
        {
            this._priceCalculator = priceCalculator;
            this._itemDa = itemDa;
        }

        public decimal GetDefaultPrice(string item)
        {
            return this._priceCalculator.GetDefaultPrice(item);
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return this._priceCalculator.GetExtendPrices(item, qty);
        }

        public List<Item> GetAllItems()
        {
            return GetItems();
        }

        public List<Item> GetAppleItems()
        {
            return GetItems(ItemHelper.IsAppleLabel);
        }

        public List<Item> GetSamsungItems()
        {
            return GetItems(ItemHelper.IsSamsungLabel);
        }

        public List<Item> GetItems(Func<Item, bool> predicate = null)
        {
            var items = this._itemDa.GetItems();
            return EnumerableHelper.Where(items, predicate)?.ToList();
        }
    }
}
