﻿using System;
using System.Collections.Generic;
using System.Text;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public interface IItemDA
    {
        List<Item> GetItems();
    }
}
